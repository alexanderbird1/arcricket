import React from "react";

export const scores = ["20", "19", "18", "17", "16", "15", "D", "T", "B", "Points"];

const ScoreboardPanels = ({
    teams,
    groupIsClosed,
    pointsAvailable,
    onSubmitTeam,
    onUpdateTeam,
    onSubmitPoints,
    onUpdatePoints,
    decreaseScore,
    increaseScore,
    addTeam
}) => (
    <div className="scoreboard">
        <div className="panel scores">
            <div className="header"/>
            <div className="items">
                {scores.map((group, index) => <span className={`score item`}
                                                    key={"item-" + index}><span
                    className={`${groupIsClosed(group) ? "closed" : ""}`}>{group}</span></span>)}
            </div>
        </div>
        <div className="teams">
            {teams.map((team, teamIndex) =>
                <div className={"panel scores"} key={"team-" + teamIndex}>
                    <div className="header players">
                        <form onSubmit={onSubmitTeam} onBlur={onSubmitTeam}>
                            <textarea id={teamIndex} value={team.players.join(", ")} rows={2}
                                      className="team-scoreboard-input" onChange={onUpdateTeam}/>
                        </form>
                    </div>
                    <div className="score items">
                        {scores.map((scoreKey) => {
                            return <div className="item" key={"team-" + teamIndex + "-score-" + scoreKey}>
                                {scoreKey !== "Points" ?
                                    <span className="cell">
                                        <span className="score-button"
                                              style={{fontSize: 20, justifySelf: "flex-start"}}>
                                        <i className="fas fa-minus-circle"
                                           onClick={() => decreaseScore(teamIndex, scoreKey)}/>
                                        </span>
                                        <span className={`x ${groupIsClosed(scoreKey) ? "closed" : pointsAvailable(scoreKey, teamIndex) ? "open": ""}`}>
                                            {String("X").repeat(team["scores"][`${scoreKey}`])}
                                        </span>
                                        <span className="score-button"
                                              style={{fontSize: 20, justifySelf: "flex-end"}}>
                                        <i className="fas fa-plus-circle"
                                           onClick={() => increaseScore(teamIndex, scoreKey)}/>
                                        </span>
                                    </span>
                                    : <form onSubmit={onSubmitPoints}>
                                        <input className={"points-input"} type="number" id={teamIndex}
                                               value={team.scores.points}
                                               onChange={onUpdatePoints}/>
                                    </form>}
                            </div>;
                        })}
                    </div>
                </div>
            )}
            <div className={"panel scores"} key={"team-" + teams.length}>
                <div className="header players">
                    <i className="fas fa-user-plus" aria-label={"Add Team"} onClick={addTeam}/>
                </div>
                <div className="score items">
                    {scores.map((scoreKey) => {
                        return <div className="item" key={"new-team-" + scoreKey}/>;
                    })}
                </div>
            </div>
        </div>
    </div>
);
        
export default ScoreboardPanels;
