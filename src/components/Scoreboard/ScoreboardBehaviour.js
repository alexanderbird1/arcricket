/*
 * ScoreboardBehaviour provides a set of pure functions to define the business
 * logic of the Scoreboard. It does not explicitly set any state or do anything
 * specifically React-ey, so it's very easy to test. Stuff comes in, different
 * stuff goes out.
 *
 * You shouldn't need to add instance properties -- if one method uses another,
 * simply pass everything it needs to do its job as arguments. This makes it so
 * each method can be tested in isolation. (And it's easier to think about in
 * isolation.) 
 */
export default class ScoreboardBehaviour {
  addByMultiplier(hit, multiplier) {
    switch(multiplier) {
      case 'D': return hit * 2;
      case 'T': return hit * 3;
      default: return 0;
    }
  }

  groupIsClosed(teams, key) {
    if(key === 'Points') return false;
    return !teams.find(team => team.scores[key] < 3);
  }

  pointsAvailable(teams, key, teamIndex) {
    if(key === 'Points') return false;
    return !this.groupIsClosed(teams, key) && teams[teamIndex].scores[key] === 3;
  }

  updateTeamPointsForKey(team, key) {
    let pointsToAdd = key === "B" ? 25 : parseInt(key);
    team.scores.points = parseInt(team.scores.points) + pointsToAdd;
    return team;
  }

  handleScoreIncrease(teams, teamIndex, key) {
    if(this.groupIsClosed(teams, key)) {
      return {};
    }
    const pointsAvailable = this.pointsAvailable(teams, key, teamIndex);
    if(pointsAvailable) {
      if(key === "T" || key === "D") {
        return { openTheModal: true };
      } else {
        teams[teamIndex] = this.updateTeamPointsForKey(teams[teamIndex], key);
        return { updateTeams: teams }
      }
    } else {
        teams[teamIndex].scores[key] += 1;
        return { updateTeams: teams }
    }
  }
}
