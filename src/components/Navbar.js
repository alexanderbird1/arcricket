import React, {Component} from "react";
import {Link} from "react-router-dom";

class Navbar extends Component {

	render() {
		return (
			<nav className="navbar navbar-expand-md navbar-dark fixed-top main-red">
				<Link className="navbar-brand btn button brand" to="/">ARCRICKET</Link>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
						aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarCollapse">
					<ul className="nav navbar-nav">
						{/*<li className="nav-item">*/}
							{/*<Link className="nav-link button" to="/leaderboard">LEADERBOARD</Link>*/}
						{/*</li>*/}
						<li className="nav-item">
							<Link className="nav-link button" to="/load-game">LOAD GAME</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link button" to="/new-game">NEW GAME</Link>
						</li>
					</ul>
				</div>
			</nav>
		);
	}
}

export default Navbar;
