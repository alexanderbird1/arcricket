import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import '../../../App.scss';
import '../../../index.css';

import GameCard from './GameCard';

const gameData = {
  id: 'foobar',
  startDate: { toDate: () => new Date(2012, 2, 14) },
  teams: [
    { players: [ 'Joe', 'Jimmy', 'Jane' ] },
    { players: [ 'Beatrice', 'Barbara Byanka', 'B.B.' ] },
    { players: [ 'Alfred' ] },
  ]
}

storiesOf('GameCard', module)
  .add('basic', () => <GameCard
      gameData={gameData}
      isActive={true}
      endGame={action('endGame')}
      loadGame={action('loadGame')}
  />)
