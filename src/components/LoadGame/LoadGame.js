import React, {Component} from "react";
import firebase from "../../config/firebase-config";
import LoadingModal from "../modals/LoadingModal";
import GameCard from "./presentation/GameCard";

const db = firebase.firestore();

class LoadGame extends Component {
    constructor(props) {
        super(props);

        this.state = {
            games: [],
            loadingModalOpen: false,
            requestMade: false
        };
    }

    componentDidMount() {
        this.unsubscribe = this.setState({loadingModalOpen: true}, () => {
            db.collection("games")
                .onSnapshot((snapshots) => {
                    let games = [];
                    snapshots.forEach((gameDoc) => {
                        const {startDate, teams, endDate} = gameDoc.data();
                        const gameData = {
                            startDate: startDate,
                            teams: teams,
                            id: gameDoc.id
                        };
                        if (endDate) gameData["endDate"] = endDate;
                        games.push(gameData);
                    });
                    const sortedGames = games.sort((left, right) => right.startDate.seconds - left.startDate.seconds);
                    this.setState({
                        games: sortedGames,
                        loadingModalOpen: false,
                        requestMade: true
                    });
                });
        });
    };

    componentWillUnmount() {
        this.unsubsribe && this.unsubsribe();
    }

    loadGameCards = (isActive) => {
        const {games} = this.state;
        let cards = [];
        games.filter((game) => {
            const endDate = Object.keys(game).indexOf("endDate");
            return isActive ? endDate === -1 : endDate !== -1;
        }).forEach((gameData, index) => {
            cards.push(<GameCard key={`game-${index}`} isActive={isActive} gameData={gameData} endGame={this.endGame}
                                 loadGame={this.loadGame}/>);
        });
        return cards;
    };

    loadGame = (gameId) => {
        this.props.history.push(`/game/${gameId}`);
    };

    endGame = (gameId) => {
        const game = this.state.games.find((game) => game.id === gameId);
        if (game) {
            this.setState({loadingModalOpen: true}, () => {
                db.collection("games")
                    .doc(game.id)
                    .update({endDate: new Date()})
                    .then(() => this.setState({loadingModalOpen: false}))
                    .catch((error) => {
                        console.log(error);
                        this.setState({loadingModalOpen: false});
                    });
            });
        }
    };

    render() {
        const activeGames = this.loadGameCards(true);
        activeGames.unshift(<div className="game-card" key={`new-game-card`}>
            <div className={"new-game-card"}
                 onClick={() => this.props.history.push("/new-game")}>
                <b>Start new game</b>
                <i className="fas fa-plus-circle fa-3x"/>
            </div>
        </div>);

        const inactiveGames = this.loadGameCards(false);
        return (
            <div>
                <LoadingModal isVisible={this.state.loadingModalOpen} text={"Loading games..."}/>
                {!this.state.requestMade ? null :
                    this.state.games.length === 0 ?
                        <h1>No games found</h1> :
                        <div style={{width: "75%", flexDirection: "column", margin: "0 auto"}}>
                            {activeGames.length > 0 ? <div>
                                <h1>Active Games</h1>
                                <div className="game-cards">
                                    {activeGames}
                                </div>
                            </div> : null}
                            {inactiveGames.length > 0 ? <div>
                                <h1>Inactive Games</h1>
                                <div className="game-cards">
                                    {inactiveGames}
                                </div>
                            </div> : null}
                        </div>
                }
            </div>
        );
    }
}

export default LoadGame;