import React, {Component} from "react";
import Modal from "react-modal";
import GeneralModal from "./modals/GeneralModal";
import SubmitModal from "./modals/SubmitModal";
import LoadingModal from "./modals/LoadingModal";
import firebase from "../config/firebase-config";
import classnames from "classnames";

Modal.setAppElement("#root");

const db = firebase.firestore();

class NewGame extends Component {
    constructor(props) {
        super(props);

        this.state = {
            players: [],
            teams: [[]],
            newPlayer1: "",
            submitModalOpen: false,
            submitError: false,
            loadingModalOpen: false,
            gameId: null,
            teamErrors: [false],
            successModalOpen: false,
            errorModalOpen: false,
        };
    }

    onPlayerInputChange = (e, index) => {
        const {teamErrors} = this.state;
        teamErrors[index] = this.validatePlayer(e.target.value);
        this.setState({[`newPlayer${index + 1}`]: e.target.value, teamErrors});
    };

    onClickSubmitTeams = (e) => {
        e.preventDefault();
        const {teams} = this.state;
        for (let i = this.state.teams.length - 1; i >= 0; i--) {
            if (this.state.teams[i].length === 0) {
                teams.splice(i, 1);
            }
        }
        this.setState({teams}, () => {
            const teamsAreValid = this.validateTeams();
            this.setState({submitError: !teamsAreValid, submitModalOpen: teamsAreValid});
        });
    };

    validateTeams = () => {
        const {teams, teamErrors} = this.state;
        teams.forEach((team, index) => {
            const teamError = team.length === 0;
            teamErrors.splice(index, 1, teamError);
        });
        this.setState({teamErrors});
        return teamErrors.reduce((acc, cur) => acc && !cur, true);
    };

    validatePlayer = (playerName) => {
        // Player names can contain only a-z or A-Z, and may have one or two names
        // Good: "Everett Blakley", "Everett", "Everett B"
        // Bad: "Hitler!", "John Jacob Jingleheimer Schmidt", "C3PO",
        return playerName.search(/^(\b[a-zA-Z]+|\b[a-zA-Z]+\s[a-zA-Z]+\b)$/gmiu) === -1;
    };

    addPlayerToTeam = (e, teamIndex) => {
        e.preventDefault();
        let team = this.state.teams[teamIndex];
        team.push(this.state[`newPlayer${teamIndex + 1}`]);
        let {teams} = this.state;
        teams.splice(teamIndex, 1, team);
        this.setState({teams, [`newPlayer${teamIndex + 1}`]: ""}, () => {
            const teamsAreValid = this.validateTeams();
            this.setState({submitError: !teamsAreValid});
        });
    };

    addTeam = (e) => {
        e.preventDefault();
        this.setState((prevState) => ({
            teams: [...prevState.teams, []],
            [`newPlayer${prevState.teams.length + 1}`]: "",
            teamErrors: [...prevState.teamErrors, false]
        }));
    };

    removePlayer = (teamIndex, playerIndex) => {
        let {teams} = this.state;
        let team = teams[teamIndex];
        team.splice(playerIndex, 1);
        teams.splice(teamIndex, 1, team);
        this.setState({teams});
    };

    submitTeams = () => {
        this.setState({submitModalOpen: false}, () => {
            this.setState({loadingModalOpen: true}, () => {
                db.collection("games").add({
                    teams: this.state.teams.map((team, index) => {
                        return {
                            id: index,
                            players: team,
                            scores: {
                                "20": 0,
                                "19": 0,
                                "18": 0,
                                "17": 0,
                                "16": 0,
                                "15": 0,
                                "T": 0,
                                "D": 0,
                                "B": 0,
                                "points": 0
                            }
                        };
                    }),
                    startDate: new Date()
                }).then((gameRef) => {
                    this.setState({loadingModalOpen: false, gameId: gameRef.id}, () => {
                        this.setState({successModalOpen: true, errorModalOpen: false}, () => {
                            this.closeModalsAndRedirect(true);
                        });
                    });
                }).catch(err => {
                    this.setState({loadingModalOpen: false}, () => {
                        console.log(err);
                        this.setState({successModalOpen: false, errorModalOpen: true});
                    });
                });
            });
        });
    };

    closeModalsAndRedirect = (redirect) => {
        setTimeout(() => {
            this.setState({
                successModalOpen: false, errorModalOpen: false, loadingModalOpen: false
            }, () => {
                if (redirect && this.state.gameId) {
                    this.props.history.push(`/game/${this.state.gameId}`);
                }
            });
        }, 1000);

    };

    render() {
        return <div className="new-game">
            <SubmitModal isOpen={this.state.submitModalOpen} teams={this.state.teams} onSubmit={this.submitTeams}
                         onCancel={() => this.setState({submitModalOpen: false})}/>
            <LoadingModal isVisible={this.state.loadingModalOpen} text={"Creating your game..."}/>
            <GeneralModal isOpen={this.state.successModalOpen} text={"You will be re-directed shortly"}
                          title={"Game successfully created!"}/>
            <GeneralModal isOpen={this.state.errorModalOpen} text={"Error in creating game"}
                          title={"Check the dev console for more information"}/>
            <h1>New Game!</h1>
            <h5 className="mb-3">Add teams and add players to teams</h5>
            <p>Player names can contain up to two names (i.e. First Last, First, First L), with no special characters or
                numbers</p>
            <form className="add-players" noValidate>
                {this.state.teams.map((team, teamIndex) => (
                    <div className={"team-form"} key={"team-form-" + teamIndex}>
                        <div className="label">
                            Team {teamIndex + 1}
                        </div>
                        <div className="players">
                            {team.length > 0 ?
                                <div className="existing-players">
                                    {team.map((player, playerIndex) =>
                                        <span className="btn existing-player"
                                              key={"player-" + teamIndex + "-" + playerIndex}>{player + "  "}<i
                                            className="fas fa-times"
                                            onClick={() => this.removePlayer(teamIndex, playerIndex)}/></span>)}
                                </div> : null}
                            <div className="new-player">
                                <input type="text" id="player-name"
                                       className={classnames("form-control name-input", {"is-invalid": this.state.teamErrors[teamIndex]})}
                                       value={this.state[`newPlayer${teamIndex + 1}`]}
                                       onChange={(e) => this.onPlayerInputChange(e, teamIndex)}
                                       placeholder="Enter New Player name"/>
                                <button
                                    className={classnames("btn add-player",
                                        this.state[`newPlayer${teamIndex + 1}`] === "" || this.state.teamErrors[teamIndex]
                                            ? "dark-grey" : "dark-red")}
                                    disabled={this.state[`newPlayer${teamIndex + 1}`] === "" || this.state.teamErrors[teamIndex]}
                                    onClick={(e) => this.addPlayerToTeam(e, teamIndex)}><span className="add-player">Add Player</span><i
                                    className="fas fa-user-plus"/></button>
                            </div>
                            {this.state.teamErrors[teamIndex] ?
                                <div className="error invalid-feedback">There is an error with this player
                                    name</div> : null}
                        </div>
                    </div>
                ))}
                <div className={"team-form"}>
                    <button className="btn dark-red add-team" onClick={(e) => this.addTeam(e)}>Add Team <i
                        className="fas fa-users"/></button>
                </div>
            </form>
            <div className="submit">
                <button className="btn dark-red submit" style={{marginTop: 10}}
                        onClick={(e) => this.onClickSubmitTeams(e)}>Submit Teams <i
                    className="fas fa-check-square"/></button>
                <br/>
                {this.state.submitError ? <small style={{color: "#A0272B"}}>Please enter at least one player before
                    submitting</small> : null}
            </div>
        </div>;
    }
}

export default NewGame;
