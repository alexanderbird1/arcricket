import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import '../../App.scss';
import '../../index.css';

import ScoreModal from './ScoreModal';

storiesOf('ScoreModal', module)
  .addDecorator(withKnobs)
  .add('basic', () => <ScoreModal
    isOpen={boolean('isOpen', true)}
    multiplier={text('multiplier', 'D')}
    onCancel={action('onCancel')}
    addByMultiplier={action('addByMultiplier')}
  />)
