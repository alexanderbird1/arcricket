import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';

import '../../App.scss';
import '../../index.css';

import LoadingModal from './LoadingModal';

storiesOf('LoadingModal', module)
  .addDecorator(withKnobs)
  .add('basic', () => <LoadingModal isVisible={boolean('isVisible', true)} text={text('text', 'It approaches')} />)
