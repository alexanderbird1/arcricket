import firebase from "firebase/app";
import "firebase/firestore"

const config = {
	apiKey: "AIzaSyAUt71cWLwhtrSLs71tXuvgf2WnBbbrzzc", //process.env.REACT_APP_FIREBASE_KEY,
	authDomain: "arcricket-394b7.firebaseapp.com", //process.env.REACT_APP_FIREBASE_DOMAIN,
	databaseURL: "https://arcricket-394b7.firebaseio.com", //process.env.REACT_APP_FIREBASE_DATABASE,
	projectId: "arcricket-394b7", //process.env.REACT_APP_FIREBASE_PROJECT_ID,
	storageBucket: "arcricket-394b7.appspot.com", //process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
	messagingSenderId: "743147833036" //process.env.REACT_APP_FIREBASE_SENDER_ID
};

const app = firebase.initializeApp(config);

export default app;
