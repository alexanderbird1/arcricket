import React, {Component} from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import "./App.scss";

import Navbar from "./components/Navbar";
import Landing from "./components/Landing"
import NewGame from "./components/NewGame"
import Scoreboard from "./components/Scoreboard"
import Leaderboard from "./components/Leaderboard"
import LoadGame from "./components/LoadGame/LoadGame"

class App extends Component {
	render() {
		return (
			<Router>
				<Navbar/>
				<Route exact path="/" component={Landing} />
				<Route path="/new-game" component={NewGame} />
				<Route path="/game/:id" component={Scoreboard} />
				<Route path="/leaderboard" component={Leaderboard} />
				<Route path="/load-game" component={LoadGame} />
			</Router>
		);
	}
}

export default App;
